
const {app,server} = require ('./index');
const request = require ('supertest');


test('La respuesta debe ser un JSON que contiene un array de productos',
    async () => {
        const response = await request(app)
            .get('/pruebatest')
            // Verifica que la respuesta tenga un código de estado 200
            .expect(200) ;
            console.log(response.body); // Agrega este console.log para mostrar el JSON retornado


    });

test('El test debe controlar la longitud del array',
    async () => {
        const response = await request(app)
            .get('/pruebatest')
            .expect(200) ;// Verifica que la respuesta tenga un código de estado 200
            const jsonStr = JSON.stringify(response.body); // Convierte el JSON a una cadena
            console.log('Longitud de caracteres del JSON:', jsonStr.length); // Muestra la longitud de caracteres

            // Verifica que la longitud del array sea igual a 3
            expect(response.body.length).toBe(3);



    });

test('El test debe controlar que el primer nombre de Producto sea igual al proporcionado',
    async () => {
        const response = await request(app)
            .get('/pruebatest')
            .expect(200) ;// Verifica que la respuesta tenga un código de estado 200
            // Verifica que el primer nombre sea igual a "Coca Cola 500ml"
            expect(response.body[0].nombre).toBe('Coca Cola 500ml');


    });

test('El test debe controlar que el segundo nombre de Producto sea igual al proporcionado',
    async () => {
        const response = await request(app)
            .get('/pruebatest')
            .expect(200) ;// Verifica que la respuesta tenga un código de estado 200
            // Verifica que el primer nombre sea igual a ""Pepsi 500ml""
            expect(response.body[1].nombre).toBe('Pepsi 500ml');


    });



        afterAll(() => {
            server.close();
        });

