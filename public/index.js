const express = require("express");
const app = express();
const slugify = require("slugify");
const path = require('path');

const titulo = 'UG0281';

const productos = [{id: 1, nombre:"Coca Cola 500ml", precio: 15.10},
    {id: 2, nombre:"Pepsi 500ml", precio: 12.05},
    {id: 3, nombre:"Fanta 250ml", precio: 10.17}]
productos.forEach(producto => {
    producto.slug = slugify(producto.nombre);
});

app.use(express.static('public'));
app.set("view engine","ejs");
app.set('views', path.join(__dirname, 'views'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


//peticion get
app.get("/", (req, res) => {
//res.send("Hola mundo")
    res.render("bienvenido", {title:titulo, productos:productos})
});

app.get("/productos/:slug", (req, res) => {
    const slug = req.params.slug;
    const productoEncontrado = productos.find(producto => producto.slug === slug);
    res.render("productos", { title: titulo, producto: productoEncontrado });
});

app.get("/crear_producto/", (req, res) => {
    
    res.render("crearProducto", { titulo: titulo });
});

app.get("/pruebatest", (req, res) => {

    res.json(productos);
});


app.put("/productos/:slug", (req, res) => {
    const slug = req.params.slug;
    const productoEncontrado = productos.find(producto => producto.slug === slug);

    if (productoEncontrado) {
        productoEncontrado.nombre = req.body.nombre;
        productoEncontrado.precio = parseFloat(req.body.precio);

        // Actualiza los slugs antes de la redirección
        productos.forEach(producto => {
            producto.slug = slugify(producto.nombre);
        });

        res.json({ message: "Producto actualizado", slug: productoEncontrado.slug });
    } else {
        res.status(404).json({ error: "Producto no encontrado" });
    }


});

app.delete("/:id", (req, res) => {
    const p_id = Number(req.params.id);
    console.log("Contenido", typeof p_id, req.params.id);
    console.log("Array", typeof productos[0].id);

    productos.forEach((producto, index) => {
        if (producto.id == p_id) {
            console.log("indice encontrado", producto.id, p_id);
        } else {
            console.log("indice perdido", producto.id, p_id);
        }
    });

    const indexToDelete = productos.findIndex((producto) => producto.id == p_id);
    console.log("Indice", indexToDelete);
    if (indexToDelete !== -1) {
        // Actualizar los valores del producto
        const elementoEliminado = productos.splice(indexToDelete, 1);

        console.log("Producto Eliminado:", elementoEliminado);
    } else {
        console.log("No se encontró el producto con el ID especificado.");
    }
});



app.post("/", (req, res) => {
    console.log("cuerpo", req.body);
    let producto_nuevo = {
        id: 0,
        nombre: "",
        precio: 0,
        slug: "",
    };
    producto_nuevo.nombre = req.body.nombre;
    producto_nuevo.precio = req.body.precio;
    producto_nuevo.slug = slugify(req.body.nombre);
    
    const id = Math.max(...productos.map((producto) => producto.id));
    console.log("Maximo id", id);
    if (id < 0) {
        producto_nuevo.id = 1;
    } else {
        producto_nuevo.id = id + 1;
    }
    console.log("nuevo id", id);
    productos.push(producto_nuevo);
    

    res.redirect("/");
});

// Inicia el servidor nuevo comentario
const server = app.listen(3000, () => {
    console.log("Servidor puerto 3000");
});
module.exports = {app, server};